''' 
@file       Lab0x01.py
@brief      A file containing Lab 1
@details    A file that calculates values from the fibonacci sequence
@author Kevin Lee
@date September 29, 2020
'''


class Fib_Calc:
    '''
    @brief      Class containing a fibonacci calculation
    @details    Fib_Calc is a class that cointains a method that calculates values in the fibonnacci sequence.
                It should run faily quickly for indexes of about 100 or 1000
    '''
    
    def __init__(self):
        '''
        @brief This constructor runs when a Fib_Calc object is created
        '''
        
        #no vars
        pass
    
    def fib():
        ''' 
        @brief      Method that asks for input index and calculates a fibonacci number
        @details    This method calculates a Fibonacci number corresponding to
                    a specified index. 
                    This is modified to ask for user input instead of passing it idx.
        '''
        
        print('Please input an index to calculate it\'s Fibonacci Number')
        idx = input('Index: ')
        if idx.isnumeric():
            idx = int(idx)
        else:
            print('Invalid Index: Please try something more reasonable')
            return #case if user input is invalid
        print ('Calculating Fibonacci number at '
               'index n = {:}.'.format(idx))
        
        if idx == 0 or idx ==1:
            print(idx)
            return
        ans = 1
        prev_num = 1
        for i in range(2, idx):
            temp_num = ans
            ans += prev_num
            prev_num = temp_num
        print(ans)
         
    if __name__ == '__main__': #only is run if green arrow pressed
        fib()