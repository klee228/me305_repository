"""
@file Lab0x06_main.py
@brief A file that creates two MotorDriver Objects
@details A file that creates two MotorDriver Objects for the given hardware
@author Kevin Lee
@date November 10, 2020
"""

import Lab0x06

##motor 1
mot1 = Lab0x06.MotorDriver('A15', 'B4', 'B5', 3,1,2)
##motor 2
mot2 = Lab0x06.MotorDriver('A15', 'B0', 'B1', 3,3,4)