"""
@file Lab0x06_front.py
@brief A file that is to be run in Spyder
@details A file that creates a User Object to communicate with the 
microcontroller from the computer
@author Kevin Lee
@date November 24, 2020
"""
import Lab0x04_6
##User object
Task = Lab0x04_6.User(10) #creates the User object
while Task.task_complete == False:
    Task.run()