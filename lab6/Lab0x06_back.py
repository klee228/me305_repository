"""
@file Lab0x06_back.py
@brief A file meant to be run on the microcontroller
@details A file that creates a ClosedLoop object
@author Kevin Lee
@date November 24, 2020
"""
from Lab0x06_Loop import ClosedLoop
## ClosedLoop Object
cl = ClosedLoop(5)
while True:
    cl.run()