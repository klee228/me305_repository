"""
@file Lab0x06.py
@brief A file containing Lab 6 program
@details A file that contains the motor driver
@author Kevin Lee
@date November 10, 2020
"""
import pyb

class MotorDriver:
    '''
    @brief A class that controls a motor
    @details A class that controls a motor through pwm
    '''
    
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer, ch1, ch2):
        '''
        @brief constructor for the MotorDriver
        @details constructor that sets up the pins and timers
        @param nSLEEP_pin string for nSLEEP location ie) 'A15'
        @param IN1_pin string for IN1 location ie) 'B4'
        @param IN2_pin string for IN2 location ie) 'B5'
        @param timer int for timer ie) 3
        @param ch1 int for 1st timer ie) 1
        @param ch2 int for 2nd timer ie) 2
        '''
        ## nSLEEP pin
        self.nSLEEP_pin = pyb.Pin(eval("pyb.Pin.cpu."+nSLEEP_pin), pyb.Pin.OUT_PP)
        ## IN1
        self.IN1_pin = pyb.Pin(eval("pyb.Pin.cpu."+IN1_pin))
        ## IN1
        self.IN2_pin = pyb.Pin(eval("pyb.Pin.cpu."+IN2_pin))
        ## Timer
        self.tim = pyb.Timer(timer,freq=20000) 
        ## channel 1
        self.tch1 = self.tim.channel(ch1,pyb.Timer.PWM, pin = self.IN1_pin)
        self.tch1.pulse_width_percent(0)
        ## channel 2
        self.tch2 = self.tim.channel(ch2,pyb.Timer.PWM, pin = self.IN2_pin)
        self.tch2.pulse_width_percent(0)
        
        print('Creating a motor driver')
        
    def enable (self):
        '''
        @brief enables the motor
        @details powers nSLEEP
        '''
        self.nSLEEP_pin.high()
        print('Enabling Motor')
        
    def disable (self):
        '''
        @brief disables the motor
        @details disables nSLeep, IN1, and IN2
        '''
        self.nSLEEP_pin.low()
        self.tch1.pulse_width_percent(0)
        self.tch2.pulse_width_percent(0)        
        print('Disabling Motor')
        
    def set_duty(self,duty):
        '''
        @brief sets duty cycle
        @details sets the duty cycle for the motor, negative numbers indicate 
        reverse direction
        @param duty int between -100 and 100 that determines the duty cycle of 
        the motor
        '''
        if duty>=-100 and duty <=100:
            ##placeholder value for pwm1
            self.val1 = 0
            ##placeholder value for pwm2
            self.val2 = 0
            if duty >0:
                self.val1 = duty
                self.val2 = 0
            else:
                self.val1 = 0
                self.val2 = -duty
            self.tch1.pulse_width_percent(self.val1)
            self.tch2.pulse_width_percent(self.val2)
            print("duty set to: "+str(duty))
 
