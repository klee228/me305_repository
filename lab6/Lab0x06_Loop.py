"""
@file Lab0x06_Loop.py
@brief A file containing the ClosedLoop class
@details A file that controls the interaction of DataCollect and MotorDriver
@author Kevin Lee
@date November 24, 2020
"""
from Lab0x06_mot import MotorDriver

from Lab0x04_main_6 import DataCollect

import utime
    
class ClosedLoop:
    '''
    @brief a class that controls the microcontroller
    @details a class that controlls the interaction of DataCollect and Motor Driver
    '''
    
    ## Initialization state
    S0_INIT = 0
    ## Waitinfg State
    S1_WAIT = 1
    ## COllection State
    S2_COLLECT = 2
    
    def __init__(self, interval):
        '''
        @brief constructor for the ClosedLoop object
        @details initializes the variables that control the motor and the communication
        @param interval int in millisecconds that controlls timing
        '''
        
        ##  The amount of time in milliseconds between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        ## time of the start of data collection
        self.collect_start = utime.ticks_ms()
        
        ##motor 1
        self. mot1 = MotorDriver('A15', 'B4', 'B5', 3,1,2)
        ##motor 2
        self.mot2 = MotorDriver('A15', 'B0', 'B1', 3,3,4)
        self.mot1.disable()
        self.mot2.disable()
        
        self.mot1.enable()
        self.mot1.set_duty(0)
        
        ## DataCollect Object
        self.task = DataCollect(interval) #creates DataCollect Object
        
        ##Kp
        self.Kp = 0
        ## reference angular velocity
        self.wref = 600 #rpm
        
        ##conversion factor from pulses to rpm
        self.conversion_factor = 1000.0/interval*60/4000
        ##current state
        self.state = self.S0_INIT 
        ##time in millis
        self.max_time = 3000
        
        
    def run(self):
        '''
        @brief finite state machine that controls the motor and the data collection
        @details modulates power to motor based on Kp
        '''
        ## current time in millis
        self.current_time = utime.ticks_ms()
        if utime.ticks_diff(self.current_time,self.next_time) >=0:
            self.task.run()
            
            if self.state == self.S0_INIT:
                self.state = self.S1_WAIT
                
            elif self.state == self.S1_WAIT:
                if self.get_Kp() >0:
                    self.task.start_data_collection()
                    self.collect_start = utime.ticks_ms()
                    self.state = self.S2_COLLECT
                    
            elif self.state == self.S2_COLLECT:
                if utime.ticks_diff(self.current_time,self.collect_start+self.max_time) >=0:
                    self.mot1.set_duty(0)
                else:
                    self.update()
                
            self.next_time = utime.ticks_add(self.current_time,self.interval)
            
        
    def update(self):
        '''
        @brief updates the duty cycle of the motor
        '''
        self.mot1.set_duty(self.get_L())
    
    def get_Kp(self):
        '''
        @brief returns Kp
        '''
        return self.task.get_Kp()
    
    def set_Kp(self, Kp):
        '''
        @brief sets Kp
        @param Kp input Kp value
        '''
        self.Kp = Kp
        
    def get_L(self):
        '''
        @brief returns L
        @details returns a value between -100 and 100 that depends on Kp and 
        the angular velocity
        '''
        self.Kp = self.task.get_Kp()
        ## output L value
        self.L = self.Kp*(self.wref-self.task.get_v()*self.conversion_factor)
        self.L = max(-100, self.L)
        self.L = min(self.L, 100)
        return self.L