"""
@file Lab0x02.py
@brief A file containing Lab 2
@details A file that controls a virtual and a physical LED at the same time
@author Kevin Lee
@date September 13, 2020
"""

import pyb
import math

class TaskVirtualLED:
    '''
    @brief      A finite state machine to control a "virtual" LED
    @details    This class implements a finites state machine to control a
                virtual LED between two states.
    '''
    ## Constant defining the initialization state
    S0_INIT = 0
    ## Constant defining the OFF state
    S1_OFF = 1
    ## Constant defining the ON state
    S2_ON = 2
    
    
    def __init__(self, period):
        '''
        @brief This constructor runs when a TaskVirtualLED object is created
        @details Constructor initializes timing variables and sets the state 
        to the initaializing state
        @param period period of one cycle in millisecconds
        '''
        ## Period in millisecconds
        self.period = period
        ## current time in milliseconds
        self.current_time = 0
        ## the time of the next state transition
        self.next_time = 0
        ## the initial time
        self.start_time = pyb.millis()
        ## the current state
        self.state = self.S0_INIT
        
    def run(self):
        '''
        @brief Runs one iteration of the virtual LED task
        @details Transitions between 3 states, S0_INIT, S1_ON, and S2_OFF to 
        control a virtual LED
        '''
        self.current_time = self.normalized_time()
        if self.current_time >= self.next_time:
            if (self.state == self.S0_INIT):
                print("initializing Virtual LED")
                self.state = self.S1_OFF
                print("LED OFF")
            elif (self.state == self.S1_OFF):
                self.state = self.S2_ON
                print("LED ON")
            elif (self.state == self.S2_ON):
                self.state = self.S1_OFF
                print("LED OFF")           
            
            self.next_time = self.normalized_time()+self.period/2.0 
    
    def normalized_time(self): #bc microcontroller hates big nums
        '''
        @brief Method returns time relative to the start time
        @details Intended to prevent any errors caused by time values being 
        excessively large
        '''
        return pyb.millis()-self.start_time
        

class TaskLED:
    '''
    @brief      A finite state machine to control the brightness of an LED
    @details    This  class impements a finite state machine to to control the 
                brightness of an LED with a sin wave shape.
    '''
    
    ## Constant defining the maximum value for LED power
    VM =100
    ## Constant defining the initialization state
    S0_INIT = 0
    ## Constant defining the LED control state
    S1_LED = 1
    
    def __init__(self, period, interval):
        '''
        @brief creates a TaskLED  object
        @details Initializes the timing and LED brightness variables
        '''
        ##brightness of the LED, values: (0-100)
        self.pwr = 0
        ## period in millisecconds
        self.period = period
        ## time between state transitions in milliseconds
        self.interval = interval
        ## current time in milliseconds
        self.current_time = 0
        ## the time of the next state transition in milliseconds
        self.next_time = 0
        ##the initial time in milliseconds
        self.start_time = pyb.millis()
        ## the current state
        self.state = self.S0_INIT
        ## designated pin of the on board LED
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        ## The frequency for the LED
        self.tim2 = pyb.Timer(2, freq = 20000)
        ## The channel for the LED
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        
    
    def run(self):
        '''
        @brief Runs one itteration of the physical LED task
        @details Transitions between states S0_INIT and S1_LED. 
        '''
        self.current_time = self.normalized_time()
        if self.current_time >= self.next_time:
            
            if (self.state == self.S0_INIT):
                print("initializing LED")
                
                self.state = self.S1_LED
            elif (self.state == self.S1_LED):
                
                self.pwr = self.VM/2+self.VM/2*math.sin(2*math.pi*self.current_time/self.period)
                self.set_LED()
                
                self.state = self.S1_LED 
            
            self.next_time = self.current_time+self.interval
            
            
        
    
    def set_LED(self):
        '''
        @brief sets the brightness of the LED
        @details Uses pulse width to control the brightness of the LED
        '''
        # print(self.pwr)
        self.t2ch1.pulse_width_percent(self.pwr)
    
    def normalized_time(self): #bc microcontroller hates big nums
        '''
        @brief Method returns time relative to the start time
        @details Intended to prevent any errors caused by time values being 
        excessively large
        '''
        return pyb.millis()-self.start_time
    
    
    

        
