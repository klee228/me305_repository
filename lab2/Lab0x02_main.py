"""
@file Lab0x02_main.py
@brief A file containing Lab 2 main program
@details A file that controls a virtual and a physical LED at the same time
@author Kevin Lee
@date September 13, 2020
"""

from Lab0x02 import TaskVirtualLED, TaskLED
## The virtual LED Task
task1 = TaskVirtualLED(5000)
## The physical LED Task
task2 = TaskLED(10000,100)

for i in range(0,1000000000): #Runs 1B cycles, stops eventually
    task1.run()
    task2.run()