"""
@file Lab0x04_main.py
@brief A file containing Lab 4 main program
@details A file that runs on the microcontroller and records encoder data that
 is sent to the computer
@author Kevin Lee
@date October 26, 2020
"""

import pyb
import utime
from pyb import UART
import array


class DataCollect:
    '''
    @brief Class that collects data from the encoder
    @details Class communicates with the computer and records and sends
    encocder data.
    '''
    
    ## State 0 initialization
    S0_INIT = 0
    
    ## Waiting state
    S1_WAIT = 1
    
    ## Response State
    S2_RESPOND = 2
    
    ## Constant defining the period for the encoder
    PERIOD = 0xfff
    
    
    def __init__(self, interval):
        '''
        @brief constructor for the DataCollect object
        @details initializes the variables
        @param interval int interval in millis

        Parameters
        ----------
        interval : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        '''
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
                
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## UART
        self.myuart = UART(2)
        
        ## stores the input character
        self.val = 0
        
        #MOTOR SHIT
        ## current encoder position
        self.curr_pos=0  #0->PERIOD-1
        ## previous  encoder position
        self.prev_pos =0 #0->PERIOD-1
        ## number of full rotations of the encoder
        self.cycles = 0  #offset tracker
        ## pin 1
        self.pin1 = 'A6'
        ## pin 2
        self.pin2 = 'A7'
        ## timer
        self.timer = 3
        ## timer object
        self.tim = pyb.Timer(self.timer)
        ## position array in ticks
        self.arrp = array.array("i")
        ## time array in millis
        self.arrt = array.array("i")
        ## determines if data is being collected
        self.collecting = False
        ## time of the start of data collection
        self.collect_start = 0

    def run(self):
        '''
        @brief finite state machine that controls comunication with the computer
        @details finite state machine that communicates with the pc and controls 
        data collection from the encoder
        '''
        ## current time in millis
        self.current_time = utime.ticks_ms()
        if utime.ticks_diff(self.current_time,self.next_time) >=0:
            
            if(self.state == self.S0_INIT):
                #state 0
                self.tim.init(prescaler=0, period = self.PERIOD)
                self.tim.channel(1, pin=eval("pyb.Pin.cpu."+self.pin1),mode = pyb.Timer.ENC_AB)
                self.tim.channel(2, pin=eval("pyb.Pin.cpu."+self.pin2),mode = pyb.Timer.ENC_AB) 
                self.state = self.S1_WAIT
            elif(self.state == self.S1_WAIT):
                #state 1
                if self.myuart.any() != 0:
                    self.val = self.myuart.readchar()
                    self.state = self.S2_RESPOND
           
            elif(self.state == self.S2_RESPOND):
                #state 2
                self.myuart = UART(2)
                if self.val==103: #G
                    if self.collecting==False:
                        self.myuart.write("data collection started")
                        self.collecting = True
                        self.arrp = array.array("i")
                        self.arrt = array.array("i")
                        self.collect_start = utime.ticks_ms()
                    else:
                        self.myuart.write("data collection already started")
                elif self.val==115: #S
                    if self.collecting == True:
                        self.myuart.write("data collection stopped")
                        self.collecting = False      
                    else:
                        self.myuart.write("data collection already stopped")
                elif self.val==111: #O
                    if self.collecting == True: 
                        self.myuart.write("data collection in progress") 
                    else:
                        self.myuart.write("p: " + str(self.arrp) + " t: " + str(self.arrt))       
                else:
                    self.myuart.write("invalid input")    
                self.state = self.S1_WAIT
            
            self.next_time = utime.ticks_add(self.current_time,self.interval)
            if self.collecting == True:
                self.add_data()

        
    
    def update(self):
        '''
        @brief Updates the position of the encoder
        @details Updates the position of the encoder and accounts for overflow
        and underflow
        '''
        self.prev_pos = self.curr_pos
        self.curr_pos = self.tim.counter()
        if self.get_delta() > self.PERIOD/2: #underflow
            self.cycles -= 1
        if self.get_delta() < -self.PERIOD/2: #overflow
            self.cycles +=1
            
    def get_position(self):
        '''
        @brief returns the position of the encoder
        '''
        return self.curr_pos+ self.PERIOD*self.cycles
            
    def get_delta(self):
        '''
        @brief returns the difference of the last 2 positions of the encoder
        '''
        return(self.curr_pos-self.prev_pos)
    
    def add_data(self):
        '''
        @brief updates and stores data in arrays
        '''
        self.update()
        if (utime.ticks_diff(self.current_time,self.collect_start)) > 10000:
            self.collecting = False
        else:
            self.arrt.append(utime.ticks_diff(self.current_time,self.collect_start))
            self.arrp.append(self.get_position())
    
## DataCollect Object
Task = DataCollect(200) #creates DataCollect Object
while True:
    Task.run()