# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 12:48:09 2020

@author: kevin
"""
from pyb import UART

myuart = UART(2)
while True:
    if myuart.any() != 0:
        val = myuart.readchar()
        myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo')
        myuart.write('hi')

