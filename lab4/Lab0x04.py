"""
@file Lab0x04.py
@brief A file containing Lab 4
@details A file that communicates with the microcontroller to read data from 
an encoder and exports it as a graph and a csv file
@author Kevin Lee
@date October 26, 2020
"""

import time
import serial
import array
import matplotlib.pyplot as plt
import csv

class User:
    '''
    @brief Class that comunicates with the microcontroller
    @details Class that uses the COM3 serial port to communicate with the 
    microcontroller. Input from this controls data collection for the  
    microcontroller and output to png and csv formats.
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waiting for character state
    S1_WAIT             = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2

    def __init__(self, interval):
        '''
        Creates a user interface task object.
        @param interval An integer number of microseconds between desired runs of the task
        '''
    
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
                
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time+ self.interval

        ## Serial port
        self.ser = serial.Serial('COM3',baudrate=115273,timeout=1)
        self.ser.close()
        
        ## position array in degrees
        self.arrp = array.array("f")
        
        ## time array in secconds
        self.arrt = array.array("f")
        
        ## boolean representing if the task has completed
        self.task_complete = False
        

    def run(self):
        '''
        @brief Runs one iteration of the task
        @details Finite state machine that sends and recieves data from the microcontroller
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = time.time()
        if self.curr_time - self.next_time >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('Press G to begin data collection for 10 sec\nPress S to stop data collection\nPress O to output data\n')
                self.transitionTo(self.S1_WAIT)
            
            elif(self.state == self.S1_WAIT):
                # Run State 1 Code
                cmd = input("input: ")
                if cmd != "":
                    self.ser.open()
                    self.ser.write(str(cmd).encode('ascii'))
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                ## data from the microcontroller
                self.output = str(self.ser.readline().decode('ascii'))
                self.ser.close()
                if self.output[0] == 'p':
                    self.string_to_array(self.output)
                    self.create_output()
                else:
                    print(self.output)
                self.output = ""
                self.transitionTo(self.S1_WAIT)
                    
            else:
                # Invalid state code (error handling)
                pass
                        
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
    def string_to_array(self, string):
        '''
        @brief converts input string to output int array
        @details only works for specific input formatting sent by nucleo
        @param string input string from nucleo
        '''
        ## stores the number from string
        self.num = 0
        ## 1 pos | -1 neg
        self.neg = 1
        ## reference for the current array
        self.arr = self.arrp
        ## conversion factors for time and position
        self.conversion_factor = 360/28 #ticks to deg for 7 pole pair
        for i in range(0,len(string)-1):
            if string[i] == "-":
                self.neg = -1
            if string[i] == "t":
                self.arr = self.arrt
                self.conversion_factor = .001 #milli to whole
            elif str(string[i]).isnumeric():
                self.num = self.num*10+int(string[i])
            if string[i] == "," or string[i] == "]":
                self.arr.append(self.num*self.neg*self.conversion_factor)
                self.num = 0
                self.neg = 1
        self.arrp.remove(0) #remove extra 0 caused by extra ,
        self.arrt.remove(0)
    
    def create_output(self):
        '''
        @brief formats the output for the data
        @details creates a png and a csv of the data, all porperly formatted
        '''
        plt.plot(self.arrt, self.arrp)
        plt.xlabel('Time (s)')
        plt.ylabel('Position (degrees)')
        plt.title('Position vs. Time for Encoder')
        plt.savefig('encoder_graph.png')

        with open('encoder_data.csv', mode='w') as encoder_file:
            ## writes to the csv
            self.value_writer = csv.writer(encoder_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for i in range(len(self.arrp)-1):
                self.value_writer.writerow([self.arrp[i],self.arrt[i]])
        print('Data exported as encoder_graph.png and encoder_data.csv')
        self.task_complete = True
            
##User object
Task = User(.1) #creates the User object
while Task.task_complete == False:
    Task.run()