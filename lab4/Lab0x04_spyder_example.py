# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 12:46:14 2020

@author: kevin
"""

import serial
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)

def sendChar():
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    return myval

for n in range(1):
    print(sendChar())
    ser.close()
