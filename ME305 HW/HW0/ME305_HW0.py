'''
@file ME305_HW0.py

This file represents an elevator traveling between two floors. 
Multiple elevators will be run simultaneously

There are two buttons inside the elevator and two sensors to determine position
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control the elevator
    @details    This class implements a finite state machine to control the
                operation of an elevator
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_MOVING_DOWN  = 1
    
    ## Constant defining State 2
    S2_MOVING_UP = 2
    
    ## Constant defining State 3
    S3_STOPPED_ON_FLOOR_1 = 3
    
    ## Constant defining State 4
    S4_STOPPED_ON_FLOOR_2 = 4
    
    def __init__(self, interval, motor, button_1, button_2, first, second, task_name):
        '''
        @brief      Creates a TaskElevator object.
        '''
        
        ## The name of the task used to differentiate between different tasks durring multitasking
        self.task_name = task_name
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.motor = motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## The button object used for the floor 1 button
        self.button_1 = button_1
        
        ## The button object used for the floor 2 button
        self.button_2 = button_2
        
        ## The button object used for the first floor sensor
        self.first = first
        
        ## The button object used for the second floor sensor
        self.second = second
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT):
                print(self.task_name + " " + str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.button_1.clearButtonState()
                self.button_2.clearButtonState()
                self.motor = 2
            
            elif(self.state == self.S1_MOVING_DOWN):
                print(self.task_name + " " + str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                self.transitionTo(self.S3_STOPPED_ON_FLOOR_1)
                self.first = 1
                self.motor = 0
            
            elif(self.state == self.S2_MOVING_UP):
                print(self.task_name + " " + str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                self.transitionTo(self.S4_STOPPED_ON_FLOOR_2)
                self.second = 1
                self.motor = 0
            
            elif(self.state == self.S3_STOPPED_ON_FLOOR_1):
                print(self.task_name + " " + str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                if self.button_2.getButtonState() == 1:
                    self.transitionTo(self.S2_MOVING_UP)
                    self.motor = 1
                    self.first = 0
                    self.button_1.clearButtonState()
            
            elif(self.state == self.S4_STOPPED_ON_FLOOR_2):
                print(self.task_name + " " + str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                if self.button_2.getButtonState() == 1:
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.motor = 2
                    self.first = 0
                    self.button_2.clearButtonState()            
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                print("ERROR")
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
    
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by to change the floor
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        ## The button's state
        self.state = 0
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized 0 or 01 if the button has not been pressed yet.
        @return     1 if pressed already or a random 0 or 1
        '''
        if self.state == 0:
            self.state = choice([0,1])
        return self.state
    
    def clearButtonState(self):
        '''
        @brief      sets the button state to 0
        @detail     sets button state to 0 to signify that the elevator has recieved the request
        '''
        
        self.state = 0
        
    
class Sensor:
    '''
    @brief      A sensor class
    @details    This class represents a sensor that determines if the elevator is there
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Sensor object
        @param pin  A pin object that the sensor is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        ## The sensor's state
        self.state = 0
        
        print('Sensor object created attached to pin '+ str(self.pin))

    
    def getSensorState(self):
        '''
        @brief      Gets the Sensor's state.
        @details    Returns the sensor's state.
        @return     The sensor's state.
        '''
        return self.state

class Motor:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        ## The motor's state
        self.state = 0
        pass
    
    def Set(self, state):
        '''
        @brief Sets the motor state to the input value
        '''
        self.state = state
        print('Motor state: '+ str(self.state))
    
        
