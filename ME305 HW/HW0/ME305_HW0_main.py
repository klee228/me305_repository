'''
@file ME305_HW0_main.py
'''

from ME305_HW1 import Motor, Button, Sensor, TaskElevator
        
## Motor Object
motor_a = Motor()

## Button Object for "button 1"
button_1_a = Button('PB6')

## Button Object for "button 2"
button_2_a = Button('PB7')

## Button Object for "first"
first_a = Sensor('PB8')

## Button Object for "second"
second_a = Sensor('PB9')

## Motor Object
motor_b = Motor()

## Button Object for "button 1"
button_1_b = Button('PB10')

## Button Object for "button 2"
button_2_b = Button('PB11')

## Button Object for "first"
first_b = Sensor('PB12')

## Button Object for "second"
second_b = Sensor('PB13')

## Task objects
task1 = TaskElevator(0.1, motor_a, button_1_a, button_2_a, first_a, second_a, "task_1") # Will also run constructor

task2 = TaskElevator(0.1, motor_b, button_1_b, button_2_b, first_b, second_b, "task_2") # Will also run constructor

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()
#    task3.run()