"""
@file Lab0x07_sim.py
@brief A file containing a simulated motor and encoder using equations from lecture
@details A file that is adapted to combine teh functionality of both the motor and encoder
@author Kevin Lee
@date December 2, 2020
"""

class SimMotorEncoder:
    '''
    @brief A class that controls a simulated motor and encoder wih equations from lecture
    @details This ocmbines the functionality of the motor and encoder driver classes from previous labs
    '''
    
    def __init__ (self):
        '''
        @brief constructor for the SimMotorEncoder
        @details constructor that sets up the  simulated pins and encoder 
        '''
        ## nSLEEP pin
        self.nSLEEP_pin = "low"
        ## channel 1
        self.tch1_pulse_width_percent = 0
        ## channel 2
        self.tch2_pulse_width_percent = 0
        
        ## Encoder counter
        self.encoder_counter = 0
        ## period
        self.PERIOD = 0xfff
        
        ##previous encoder position
        self.prev_pos = 0
        ##current encoder position
        self.curr_pos = 0
        ## counter for over/underflow
        self.cycles = 0
        
        ##motor rotational velocity rad/sec
        self.w = 0
        
        print('Creating a motor driver')
    
    def run(self):
        '''
        @brief Updates the motor's behavior using equations from lecture
        @details Uses aproximate parameters since many values are not known
        '''
        if self.nSLEEP_pin == "high":
            ##delta T
            self.dt = 0.001
            ##composite moment of inertia
            self.J = 0.01
            ## Torque Load
            self.TL = 0.0
            ## viscous damping coefficient
            self.b = 0.1
            ## Voltage
            self.V = 5*(self.tch1_pulse_width_percent-self.tch2_pulse_width_percent)/100
            ## KT
            if self.w == 0:
                ##KT
                self.KT = self.V/1
            else:
                ##KT
                self.KT = self.V/abs(self.w)
            ## current
            self.i = 3.5
            ##change in angular velocity radians
            self.dw = self.dt/self.J*(self.TL-self.b*self.w+self.KT*self.i)
            
            self.w += max(-.2, min(.2,self.dw))
            self.encoder_counter += self.w*self.dt *4000/2/3.14
            
        self.update()

        
    def enable (self):
        '''
        @brief enables the motor
        @details powers nSLEEP
        '''
        self.nSLEEP_pin= "high"
        print('Enabling Motor')
        
    def disable (self):
        '''
        @brief disables the motor
        @details disables nSLeep, IN1, and IN2
        '''
        self.nSLEEP_pin = "low"
        self.tch1_pulse_width_percent = 0
        self.tch2_pulse_width_percent = 0        
        print('Disabling Motor')
        
    def set_duty(self,duty):
        '''
        @brief sets duty cycle
        @details sets the duty cycle for the motor, negative numbers indicate 
        reverse direction
        @param duty int between -100 and 100 that determines the duty cycle of 
        the motor
        '''
        if duty>=-100 and duty <=100:
            ##placeholder value for pwm1
            self.val1 = 0
            ##placeholder value for pwm2
            self.val2 = 0
            if duty >0:
                self.val1 = duty
                self.val2 = 0
            else:
                self.val1 = 0
                self.val2 = -duty
            self.tch1_pulse_width_percent = self.val1
            self.tch2_pulse_width_percent = self.val2
            # print("duty set to: "+str(duty))
    
    def update(self):
        '''
        @brief Updates the position of the encoder
        @details Updates the position of the encoder and accounts for overflow
        and underflow
        '''
        self.prev_pos = self.curr_pos
        self.curr_pos = self.encoder_counter
        # print("pp: " + str(self.prev_pos) + " cp: "+ str(self.curr_pos))
        if self.get_delta() > self.PERIOD/2: #underflow
            self.cycles -= 1
        if self.get_delta() < -self.PERIOD/2: #overflow
            self.cycles +=1
            
    def get_position(self):
        '''
        @brief returns the position of the encoder
        '''
        return self.curr_pos+ self.PERIOD*self.cycles
            
    def get_delta(self):
        '''
        @brief returns the difference of the last 2 positions of the encoder
        '''
        return(self.curr_pos-self.prev_pos)
    
    def get_adjusted_delta(self):
        '''
        @brief returns get_delta adjusted for under/overflow
        '''
        ## temporary delta
        self.dtemp = self.get_delta()
        if self.dtemp > self.PERIOD/2:
            return self.dtemp - self.PERIOD
        if self.dtemp < -self.PERIOD/2:
            return self.dtemp + self.PERIOD
        return self.dtemp
    
    def get_v(self):
        '''
        @brief return get_adjusted_delta converted to rpm
        '''
        return self.get_adjusted_delta()*60/4000/.001