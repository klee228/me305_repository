"""
@file Lab0x07_csv.py
@brief A file meant to decode data from the csv file
@details A file that converts the csv into two dictionaries
@author Kevin Lee
@date December 2, 2020
"""

import csv

class CSV_Reader:
    '''
    @brief converts csv into 2 dictionaries
    @details converts csv into a velocity and a position dictionary
    '''
    
    def __init__(self):
        '''
        @brief constructor for CSV_Reader 
        @details creates dictionaries from csv data
        '''
        ## velocity dictionary
        self.vdict = {}
        ## position dictionary
        self.pdict = {}

        with open('reference.csv', newline = '') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                self.vdict[float(row[0])] = float(row[1])
                self.pdict[float(row[0])] = float(row[2])
                
    def get_v(self,n):
        '''
        @brief return velocity at a given time
        @param n time between 0 and 15 sec
        '''
        if n <= 15:
            return float(self.vdict[round(n,3)])
        else:
            return 0
    
    def get_p(self,n):
        '''
        @brief return position at a given time
        @param n time between 0 and 15 sec
        '''
        if n <= 15:
            return float(self.pdict[round(n,3)])
        else:
            return 0