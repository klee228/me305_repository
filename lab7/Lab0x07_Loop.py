"""
@file Lab0x07_Loop.py
@brief A file containing the ClosedLoop class
@details A file that controls the interaction of the simulated motor and encoder
@author Kevin Lee
@date December 2, 2020
"""
from Lab0x07_sim import SimMotorEncoder

from Lab0x07_csv import CSV_Reader

import time

import array

import matplotlib.pyplot as plt

import sys
    
class ClosedLoop:
    '''
    @brief a class that controls the simulated motor and encoder
    @details a class that controlls the interaction of the simulated motor and encoder
    '''
    
    ## Initialization state
    S0_INIT = 0
    ## Collection State
    S1_COLLECT = 1
    ## Display State
    S2_DISPLAY = 2
    
    def __init__(self, interval):
        '''
        @brief constructor for the ClosedLoop object
        @details initializes the variables that control the motor and the communication
        @param interval int in secconds that controlls timing
        '''
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        ## time of the start of data collection
        self.collect_start = time.time()
        
        ##motor 1
        self.mot = SimMotorEncoder()
        self.mot.disable()
        self.mot.enable()
        self.mot.set_duty(15)
        
        ## csv reader
        self.csv = CSV_Reader()
        
        ##Kp
        self.Kp =1.25
        ## reference angular velocity
        self.wref = 0 #rpm
        ## prev L
        self.prev_L = 0
        
        ##current state
        self.state = self.S0_INIT 
        ##time in millis
        self.max_time = 15

        ## velocity array in degrees
        self.arrv = array.array("f")
        ## reference velocity array in degrees
        self.arrvref = array.array("f")
        ## position array in degrees
        self.arrp = array.array("f")
        ## position array in degrees
        self.arrpref = array.array("f")
        ## time array in secconds
        self.arrt = array.array("f")
        
        ##Performance metric x K itterations
        self.JK = 0
        ##Number of iterations
        self.K = 0

        
        
    def run(self):
        '''
        @brief finite state machine that controls the motor and the data collection
        @details modulates power to motor based on csv input file
        '''
        ## current time in millis
        self.current_time = time.time()
        if self.current_time-self.next_time >=0:
            self.mot.run()
            self.update()
            
            if self.state == self.S0_INIT:
                self.collect_start = time.time()
                print("Data Collection Started\nPlease wait 15 sec")
                self.state = self.S1_COLLECT
                
            elif self.state == self.S1_COLLECT:
                if time.time() - self.collect_start < self.max_time:
                    self.add_data()
                else:
                    self.create_output()
                    self.state = self.S2_DISPLAY
                    
            elif self.state == self.S2_DISPLAY:
                sys.exit()
                
            self.next_time = self.current_time + self.interval
            
        
    def update(self):
        '''
        @brief updates the duty cycle of the motor
        '''
        self.mot.set_duty(self.get_L())
    
    def get_Kp(self):
        '''
        @brief returns Kp
        '''
        return self.Kp
    
    def set_Kp(self, Kp):
        '''
        @brief sets Kp
        @param Kp input Kp value
        '''
        self.Kp = Kp
        
    def get_L(self):
        '''
        @brief returns L
        @details returns a value between -100 and 100 that depends on Kp and 
        the angular velocity
        '''
        ## output L value
        self.L = self.Kp*(self.wref-self.mot.get_adjusted_delta()/.001*60/4000)
        self.L = max(-100, self.L)
        self.L = min(self.L, 100)
        return self.L

    def create_output(self):
        '''
        @brief formats the output for the data
        @details creates a png of the data for position and velocity compared to the reference values, all porperly formatted
        '''
        fig, (ax1, ax2) = plt.subplots(2)
        plt.subplots_adjust(hspace = .75)
        ax1.plot(self.arrt, self.arrv)
        ax1.plot(self.arrt, self.arrvref)
        ax1.set(xlabel = 'Time (s)', ylabel = 'Angular Velocity (rpm)')
        ax1.set_title('Angular Velocity vs. Time')
        ax2.plot(self.arrt, self.arrp)
        ax2.plot(self.arrt, self.arrpref)
        ax2.set(xlabel = 'Time (s)', ylabel = 'Angle (degrees)')
        ax2.set_title('Angle vs. Time')
        fig.savefig('encoder_graph.png')
        print('Data exported as encoder_graph.png')
        print('Performance Metric: ' + str(self.JK/self.K))
        
    def perf_metric(self, dw, dx):
        '''
        @brief updates the performance metric
        @param dw float difference in angular velocity
        @param dx float difference in position
        '''
        self.JK += pow(dw,2)+pow(dx,2)
        self.K +=1
        
    def add_data(self):
        '''
        @brief adds data to the storage arrays to be displayed later
        '''
        self.arrt.append(time.time()-self.collect_start)
        ## reference velocity
        self.wref = self.csv.get_v(time.time()-self.collect_start)
        ## reference position
        self.pref = self.csv.get_p(time.time()-self.collect_start)
        self.arrvref.append(self.wref)
        self.arrpref.append(self.pref)
        self.arrv.append(self.mot.get_v())
        self.arrp.append(self.mot.get_position())
        self.perf_metric((self.wref-self.mot.get_v())*3.14/180,  (self.pref-self.mot.get_position())*3.14/180)
        