"""
@file Lab0x07.py
@brief A file that is run for the simulated motor
@details A file that creates a ClosedLoop object that controls the simulated motor
@author Kevin Lee
@date December 2, 2020
"""
from Lab0x07_Loop import ClosedLoop
## ClosedLoop Object
cl = ClosedLoop(.001)
while True:
    cl.run()