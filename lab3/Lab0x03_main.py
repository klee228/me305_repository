"""
@file Lab0x03_main.py
@brief A file containing Lab 3 main program
@details A file that reads encoder position and user input
@author Kevin Lee
@date October 19 2020
"""

from Lab0x03 import Encoder, UserInterface
## pin 1 location
p1 = "A6"
## pin 2 locaion
p2 = "A7"
## encoder timer
timer = 3

##Encoder object
TaskEnc = Encoder(p1,p2,timer, 10)
## TserInterface Object
TaskUI = UserInterface(10)


while 1==1:
    TaskEnc.run()
    TaskUI.run()