"""
@file Lab0x03.py
@brief A file containing Lab 3
@details A file that reads input from an encoder
@author Kevin Lee
@date October 18, 2020
"""
import pyb
import utime
from pyb import UART

class Encoder:
    '''
    @brief A finite state machine that controls the reading of an encoder
    @details This class implements a finite state machine to read input from 
    an encoder and interacts with the UserInterface Class
    '''
    ## Constant defining the period for the encoder
    PERIOD = 0xfff
    ## Constant defining the Update State
    S0_INIT = 0
    ## Constant defing the User Input State
    S1_UPDATE = 1
    
    def __init__(self, pin1, pin2, timer, interval):
        '''
        @brief This constructor runs when an Encoder Object is created
        @details Constructor initializes variables related to the encoder,
        internal timings, and pin input
        @param pin1 string representing the location of pin2
        @param pin2 string representing the location of pin2
        @param timer int representing timer channel
        @param interval int representing the interval in milliseconds
        '''
        ##Global variable responsible for user input
        global uiin 
        uiin = ""
        ## current encoder position
        self.curr_pos=0  #0->PERIOD-1
        ## previous  encoder position
        self.prev_pos =0 #0->PERIOD-1
        ## number of full rotations of the encoder
        self.cycles = 0  #offset tracker
        
        ## the state of the FSM
        self.state = self.S0_INIT
        ##time between instances in milli
        self.interval = int(interval) #millis
        ##current time
        self.start_time = utime.ticks_ms()
        ## time for next run
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        ## pin 1
        self.pin1 = pin1
        ## pin 2
        self.pin2 = pin2
        ## timer
        self.timer = timer
        ##timer object
        self.tim = pyb.Timer(timer)
        
    def run(self):
        '''
        @brief Updates the position of the encoder
        @details Updates the position of the encoder or performs a function 
        based on user input
        '''
        ##current time in millis
        self.current_time = utime.ticks_ms()
        if utime.ticks_diff(self.current_time, self.next_time) >= 0:
            if(self.state == self.S0_INIT):
                self.tim.init(prescaler=0, period = self.PERIOD)
                self.tim.channel(1, pin=eval("pyb.Pin.cpu."+self.pin1),mode = pyb.Timer.ENC_AB)
                self.tim.channel(2, pin=eval("pyb.Pin.cpu."+self.pin2),mode = pyb.Timer.ENC_AB)
                self.state = self.S1_UPDATE
                print("\n\nENCODER CONTROL\nz - Zero encoder position\np - Print out the encoder position\nd - Print out the encoder delta\n")

            elif(self.state == self.S1_UPDATE):
                self.update()
                if uiin:
                    if uiin ==122:
                        self.set_position(0)
                        print("Encoder position zeroed")
                    elif uiin == 112:
                        print("Encoder Position: " + str(self.get_position())) #print position
                    elif uiin == 100:
                        print("Encoder delta: "+str(self.get_delta()))#print delta
                    # print("\n\nENCODER CONTROL\nz - Zero encoder position\np - Print out the encoder position\nd - Print out the encoder delta\n")
                    uiin == None
            self.next_time = utime.ticks_add(self.current_time, self.interval)
    
    def update(self):
        '''
        @brief Updates the position of the encoder
        @details Updates the position of the encoder and accounts for overflow
        and underflow
        '''
        self.prev_pos = self.curr_pos
        self.curr_pos = self.tim.counter()
        if self.get_delta() > self.PERIOD/2: #underflow
            self.cycles -= 1
        if self.get_delta() < -self.PERIOD/2: #overflow
            self.cycles +=1
    
    def get_position(self):
        '''
        @brief returns the position of the encoder
        '''
        return self.curr_pos+ self.PERIOD*self.cycles
    
    def set_position(self, new_pos):
        '''
        @brief sets the position of the encoder
        '''
        self.curr_pos = new_pos
        self.prev_pos = new_pos
    
    def get_delta(self):
        '''
        @brief returns the difference of the last 2 positions of the encoder
        '''
        return(self.curr_pos-self.prev_pos)




class UserInterface:
    '''
    @brief A finite state machine that reads user input
    @details This class takes in user input that the Encoder class uses. 
    Modified from the provided TaskUser code.
    '''
    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_CHAR    = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2
    
    def __init__(self, interval):
        '''
        Creates a UserInterface task object.
        @param interval An integer number of microseconds between desired runs of the task
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
            
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
                
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
    
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_ms()
        global uiin
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                # Run State 1 Code
                if self.ser.any():
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    uiin = self.ser.readchar()
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                if uiin:
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    uiin = None
                    
            else:
                # Invalid state code (error handling)
                pass
                        
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)  
            
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
        
        
    
