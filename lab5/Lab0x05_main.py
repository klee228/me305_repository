"""
@file Lab0x05_main.py
@brief A file containing Lab 5 main program
@details A file that runs on the microcontroller and and communicates using 
bluetooth input
@author Kevin Lee
@date November 10, 2020
"""

from Lab0x05 import BTComms

## A bluetooth communication object
bt = BTComms()
while True:
    bt.run()