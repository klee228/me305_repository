"""
@file Lab0x05_LED_Driver.py
@brief A file containing The Driver for the LED
@details A file that runs on the microcontroller that controlls the functions 
needed to control the LED and communicate with the bluetooth device
@author Kevin Lee
@date November 10, 2020
"""

import pyb
from pyb import UART

class LEDDriver:
    '''
    @brief A class that controls the LED functions
    @details A class that sets up and controls the onboard LED and 
    communicates with the bluetooth device
    '''
    
    def __init__(self):
        '''
        @brief constructor for the LEDDriver object
        @details sets up the LED pin and the UART for Bluetooth
        '''
        ## LED pin
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        ## BT USART connection
        self.uart = UART(3,9600)
    
    def LED_ON(self):
        '''
        @brief turn on the LED
        '''
        self.pinA5.high()
    
    def LED_OFF(self):
        '''
        @brief turns off the LED
        '''
        self.pinA5.low()
    
    def read_input(self):
        '''
        @brief reads bluetooth input
        @details reads bluetooth input and only returns values between 1 and 
        10. Returns 0 if no input, or input is invalid.
        '''
        if self.uart.any()!= 0:
            ## USART input
            self.val = self.uart.readline()
            if str(self.val.decode("utf-8")).isdigit():
                if int(self.val) >0 and int(self.val) <11:
                    return int(self.val)
            print("invalid input")
            self.write_output("invalid input")
        return 0
    
    def write_output(self,out):
        '''
        @brief sends data to the bluetooth device
        '''
        self.uart.write(str(out))