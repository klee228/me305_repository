"""
@file Lab0x05.py
@brief A file containing Lab 5 main program
@details A file that runs on the microcontroller and controls the finite state 
machine
@author Kevin Lee
@date November 10, 2020
"""

import utime
from Lab0x05_LED_Driver import LEDDriver

class BTComms:
    '''
    @brief finite state machine that take input and flashes a LED
    @details finite state machine that takes bluetooth input and flashes a LED 
    at frequencies between 1 and 10 Hz
    '''
    
    ## State 0 initialization
    S0_INIT = 0
    
    ## Waiting state
    S1_WAIT = 1
    
    ## Response State
    S2_SET_LED = 2
    
    def __init__(self):
        '''
        @brief constructor for the BTComms object
        @details Initializes the BTComms object and the variables needed for 
        timing the finite state machine
        '''
        ## LED Driver Object
        self.LD = LEDDriver()
        
        ## int representing the state of the LED
        self.LED_state = 0
        
        ## The amount of time in millisecconds between runs of the task
        self.interval = 10
        
        ## Count used for LED frequency
        self.count = 0
        
        ## Value used for loops
        self.max_count = 100
        
        ## Input value
        self.val = 1
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
                
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        
    def run(self):
        '''
        @brief finite state machine that controls the LED
        @details finite state machine that controls the frequency at which a 
        LED blinks. It also communicates with the external Bluetooth device
        '''
        ## current time in millis
        self.current_time = utime.ticks_ms()
        if utime.ticks_diff(self.current_time,self.next_time) >=0:
            
            if(self.state == self.S0_INIT):
                #state 0
                self.LD.LED_OFF()
                self.state = self.S1_WAIT
                
            elif(self.state == self.S1_WAIT):
                #state 1
                self.val = self.LD.read_input()
                if self.val != 0:
                    self.state = self.S2_SET_LED
                else:
                    self.count +=1
                    if self.count >= self.max_count:
                        if self.LED_state == 0:
                            self.LD.LED_ON()
                            self.LED_state = 1
                        else:
                            self.LD.LED_OFF()
                            self.LED_state = 0
                        self.count =0
           
            elif(self.state == self.S2_SET_LED):
                #state 2
                self.max_count = 100/self.val
                print("LED blinking at " + str(self.val) + " Hz")
                self.LD.write_output("LED blinking at " + str(self.val) + " Hz")
                self.state = self.S1_WAIT
                pass
            
            self.next_time = utime.ticks_add(self.current_time,self.interval)
