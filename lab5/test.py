import pyb
from pyb import UART

pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
uart = UART(3,9600)

while True:
    if uart.any() != 0:
        val = int(uart.readline())
        if val == 0:
            print(val,' turns it OFF')
            pinA5.low()
        elif val == 1:
            print(val,' turns it ON')
            pinA5.high()

